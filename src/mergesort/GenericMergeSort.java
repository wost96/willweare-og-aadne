package mergesort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Divide and Conquer
 * left right median, sort and merge
 * big O
 */

public class GenericMergeSort{

    public static void main(String[] args) {

    }

    public static <E extends Comparable<E>> void mergeSort(E[] list) {
        if (list.length > 1 ){
            E[] firstHalf = (E[]) new Comparable[list.length / 2];
            System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
            mergeSort(firstHalf);

            int secondHalfLenght = list.length - list.length / 2;
            E[] secondHalf = (E[]) new Comparable[secondHalfLenght];
            System.arraycopy(list, list.length / 2, secondHalf, 0, secondHalfLenght);
            mergeSort(secondHalf);

            merge(firstHalf, secondHalf, list);
        }
    }


    public static <E extends Comparable<E>> void merge(E[] list1, E[] list2, E[] tempList){
        int current1 = 0;
        int current2 = 0;
        int current3 = 0;

        while ( current1 < list1.length && current2 < list2.length) {
            if (list1[current1].compareTo(list2[current2]) < 0) {
                tempList[current3++] = list1[current1++];
            } else
                tempList[current3++] = list2[current2++];
        }

        while (current1 < list1.length)
            tempList[current3++] = list1[current1++];

            while (current2 < list2.length)
                tempList[current3++] = list2[current2++];
    }

    static class IntegerComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2){
            return o1.intValue() - o2.intValue();
        }
    }

    static class StringComparator implements Comparator<String>{
        @Override
        public int compare(String o1, String o2){

            return o1.compareToIgnoreCase(o2);
        }
    }

    @SuppressWarnings("unchecked")
    public static <E> void mergeSortComp(E[] list, Comparator<? super E> comperator) {
        if (list.length > 1 ){
            E[] firstHalf = (E[]) new Comparable[list.length / 2];
            System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
            mergeSortComp(firstHalf, comperator);

            int secondHalfLenght = list.length - list.length / 2;
            E[] secondHalf = (E[]) new Comparable[secondHalfLenght];
            System.arraycopy(list, list.length / 2, secondHalf, 0, secondHalfLenght);
            mergeSortComp(secondHalf, comperator);

            mergeComp(firstHalf, secondHalf, list, comperator);
        }
    }
    public static <E> void mergeComp(E[] list1, E[] list2, E[] tempList, Comparator<? super E> comparator){
        int current1 = 0;
        int current2 = 0;
        int current3 = 0;

        while (current1 < list1.length && current2 < list2.length){
            if (comparator.compare(list1[current1], list2[current2]) < 0)
                tempList[current3++] = list1[current1++];
            else
                tempList[current3++] = list2[current2++];
        }

        while (current1 < list1.length)
            tempList[current3++] = list1[current1++];

            while (current2 < list2.length)
            tempList[current3++] = list2[current2++];
    }

}



