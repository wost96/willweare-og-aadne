package List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedListTest {
    private static MyLinkedList linkedList;
    private static MyLinkedList emptyList;
    private static MyLinkedList stringList;

    @BeforeAll
    public static void setUp(){
        linkedList = new MyLinkedList();
        emptyList = new MyLinkedList();
        stringList = new MyLinkedList();

        stringList.addFirst("six");
        stringList.addFirst("one");
        stringList.addFirst("four");
        stringList.addFirst("one");
        stringList.addFirst("one");
        stringList.addFirst("one");

        for(int i=5;i>0;i--){
            linkedList.addFirst(i);
        }
    }

    @Test
    public void contains_integer5_returnsTrue(){
        assertTrue(linkedList.contains(5));
    }

    @Test
    public void contains_integer10_returnsFalse(){
        assertFalse(linkedList.contains(10));
    }

    @Test
    public void contains_null_returnsFalse(){
        assertFalse(linkedList.contains(null));
    }

    @Test
    public void get_index0_returns1(){
        assertEquals(1,linkedList.get(0));
    }

    @Test
    public void get_index4_returns5(){
        assertEquals(5,linkedList.get(4));
    }

    @Test
    public void get_index10_returnsNull(){
        assertThrows(NullPointerException.class, ()-> {
            linkedList.get(10);
        });
    }

    @Test
    public void indexOf_Input3_returns2(){
        assertEquals(2,linkedList.indexOf(3));
    }

    @Test
    public void indexOf_Input10_returnsNegative1(){
        assertEquals(-1,linkedList.indexOf(10));
    }

    @Test
    public void indexOf_Input1_returns0(){
        assertEquals(0,linkedList.indexOf(1));
    }

    @Test
    public void lastIndexOf_Input4_returns3(){
        assertEquals(3,linkedList.lastIndexOf(4));
    }

    @Test
    public void lastIndexOf_StringOne_Returns4(){
        assertEquals(4,stringList.lastIndexOf("one"));
    }

    @Test
    public void lastIndexOf_EmptyList_ReturnsNegative1(){
        assertEquals(-1,emptyList.lastIndexOf(1));
    }


}