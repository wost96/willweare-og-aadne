package List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLOutput;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class TwoWayLinkedListTest {
    private static TwoWayLinkedList<Integer> linkedList;
    private static TwoWayLinkedList<Object> emptyList;
    private static TwoWayLinkedList<String> stringList;
    private static ListIterator<Integer> iterator;
    private static ListIterator<Integer> newIterator;

    @BeforeEach
    void setUp(){
        linkedList = new TwoWayLinkedList<>();
        emptyList = new TwoWayLinkedList<>();
        stringList = new TwoWayLinkedList<>();

        stringList.addFirst("six");
        stringList.addFirst("one");
        stringList.addFirst("four");
        stringList.addFirst("one");
        stringList.addFirst("one");
        stringList.addFirst("one");

        for(int i=5;i>0;i--){
            linkedList.addFirst(i);
        }

        iterator = linkedList.listIterator();
    }

    @Test
    @DisplayName("contains 5, returns True")
    void contains_integer5_returnsTrue(){
        assertTrue(linkedList.contains(5));
    }

    @Test
    @DisplayName("contains 10, returns False")
    void contains_integer10_returnsFalse(){
        assertFalse(linkedList.contains(10));
    }

    @Test
    @DisplayName("contains null, returns False")
    void contains_null_returnsFalse(){
        assertFalse(linkedList.contains(null));
    }

    @Test
    @DisplayName("index 0 returns 1")
    void get_index0_returns1(){
        assertEquals(1,linkedList.get(0));
    }

    @Test
    @DisplayName("index 4 returns 5")
    void get_index4_returns5(){
        assertEquals(5,linkedList.get(4));
    }

    @Test
    @DisplayName("index 10 returns null")
    void get_index10_returnsNull(){
        assertThrows(NullPointerException.class, ()-> {
            linkedList.get(10);
        });
    }

    @Test
    @DisplayName("index 3 returns 2")
    void indexOf_Input3_returns2(){
        assertEquals(2,linkedList.indexOf(3));
    }

    @Test
    @DisplayName("index 10 returns -1")
    void indexOf_Input10_returnsNegative1(){
        assertEquals(-1,linkedList.indexOf(10));
    }

    @Test
    @DisplayName("index of 1 returns 0")
    void indexOf_Input1_returns0(){
        assertEquals(0,linkedList.indexOf(1));
    }

    @Test
    @DisplayName("last index of 4 returns 3")
    void lastIndexOf_Input4_returns3(){
        assertEquals(3,linkedList.lastIndexOf(4));
    }

    @Test
    @DisplayName("last index of 'one' returns 4")
    void lastIndexOf_StringOne_Returns4(){
        assertEquals(4,stringList.lastIndexOf("one"));
    }

    @Test
    @DisplayName("last index of 1 returns -1")
    void lastIndexOf_EmptyList_ReturnsNegative1(){
        assertEquals(-1,emptyList.lastIndexOf(1));
    }

    @Test
    @DisplayName("constructs ListIterator")
    void Listiterator_Constructor_ConstructsListIterator() {
        assertEquals(iterator.getClass(), linkedList.iterator().getClass());
    }

    @Test
    @DisplayName("ListIterator works as advertised")
    void ListIterator_FunctionalityDoesNotCrashProgram() {
        iterator.next();
        iterator.next();
        iterator.next();
        assertEquals(4, iterator.next());
    }

    @Test
    @DisplayName("ListIterator throws NoSuchElement when List at start")
    void ListIterator_AtStartPrevious_ThrowsNoSuchElement() {
        newIterator = linkedList.listIterator();
        assertThrows(NoSuchElementException.class, () -> {
            newIterator.previous();
        });
    }

    @Test
    @DisplayName("ListIterator throws NoSuchElement when List is at end")
    public void ListIterator_AtEnd_ThrowsNoSuchElement() {
        // Runs through the iterator until we're done
        while(iterator.hasNext()) {
            iterator.next();
        }

        // then try to get the next (which doesn't exist).
        assertThrows(NoSuchElementException.class, () -> {
           iterator.next();
        });
    }

    @Test
    @DisplayName("ListIterator previous works as expected")
    void ListIterator_PreviousMethodWorks() {
        Integer old = iterator.next();
        assertEquals(old, iterator.previous());
    }

    @Test
    @DisplayName("ListIterator works with a given index")
    void ListIterator_WorksWithGivenIndex() {
        iterator = linkedList.listIterator(2);
        assertEquals(3, iterator.next());
    }

    @Test
    @DisplayName("ListIterator returns correct index after n next() calls")
    void ListIterator_ReturnsCorrectIndexNext() {
        iterator = linkedList.listIterator(2);
        assertEquals(2, iterator.nextIndex());
    }

    @Test
    @DisplayName("ListIterator returns correct index after n previous() calls")
    void ListIterator_ReturnsCorrectIndexPrevious() {
        iterator = linkedList.listIterator(2);
        assertEquals(1, iterator.previousIndex());
    }

    @Test
    @DisplayName("ListIterator sets correct element at correct index")
    void ListIterator_SetsCorrectElementAtCorrectIndex() {
        iterator = linkedList.listIterator(2);
        iterator.set(5);
        iterator = linkedList.listIterator(2);
        assertEquals(5, iterator.next());
    }

    @Test
    @DisplayName("ListIterator adds correct element at correct index")
    void ListIterator_AddsCorrectElementAtCorrectIndex() {
        iterator = linkedList.listIterator(2);
        iterator.add(10);
        iterator = linkedList.listIterator(2);
        assertEquals(10, iterator.next());
    }

}