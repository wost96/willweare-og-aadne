package pattern;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class PatternMatchingTest {

	@Test
	void testPatternAtEndOfString() {
		assertEquals(5, PatternMatching.find("supermega","mega"));
	}

	@Test
	void testPatternAtBeginningOfString() {
		assertEquals(0, PatternMatching.find("music is nice", "music"));
	}

	@Test
	void testPatternInMiddleOfString() {
		assertEquals(3, PatternMatching.find("ghhfdsjkhwhe123 dkk....", "fdsj"));
	}

	@Test
	void testIfPatternAlmostFinishedAtEnd() {
		assertEquals(-1, PatternMatching.find("this is a string", "string1"));
	}

	@Test
	void testIfPatternBiggerThanStringReturnsFalse() {
		assertEquals(-1, PatternMatching.find("abbdd", "abbbbcdd"));
	}

	@Test
	void testIfEmptyStringReturnsFalse() {
		assertEquals(-1, PatternMatching.find("", "sfrfwerf"));
	}

	@Test
	void testIfEmptyPatternReturnsFalse() {
		assertEquals(-1, PatternMatching.find("abc", ""));
	}

	@Test
	void testIfEmptyStringObjectReturnsFalse() {
		assertEquals(-1, PatternMatching.find(new String(), ""));
	}
}