package mergesort;


import org.junit.jupiter.api.Test;


import java.util.Arrays;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class GenericMergeSortTest {

    @Test
    public void sortListInt(){
        Integer[] li = {2, 5, 1, 7, 9, 14, 93};
        Integer[] liSort = {1, 2, 5, 7, 9, 14, 93};
        GenericMergeSort.mergeSort(li);
        assertTrue(Arrays.equals(li, liSort));
    }

    @Test
    public void checkAlreadySortedList(){
        Integer [] li = {1, 2, 3, 4, 5};
        Integer [] li1 = {1, 2, 3, 4, 5};
        GenericMergeSort.mergeSort(li);
        assertTrue(Arrays.equals(li, li1));
    }

    @Test
    public void checkIfEmptyAfterSort(){
        Integer [] li = {};
        Integer [] liEmpty = {};
        GenericMergeSort.mergeSort(li);
        assertTrue(Arrays.equals(li,liEmpty));
    }

    @Test
    public void sortListIntComparator(){

        Integer[] li1 = {1, 2, 3, 5, 7, 10};
        Integer[] li2 = {3, 5, 7, 1, 2, 10};
        GenericMergeSort.mergeSortComp(li1, new GenericMergeSort.IntegerComparator());
        GenericMergeSort.mergeSortComp(li2, new GenericMergeSort.IntegerComparator());
        assertTrue(Arrays.equals(li1,li2));
    }

    @Test
    public void SortListString(){

        String[] li1 = {"Bzer", "czer", "azer"};

        String[] l4 = {"azer","Bzer","czer"};

        GenericMergeSort.mergeSortComp(li1, new GenericMergeSort.StringComparator());


        assertArrayEquals(li1,l4);
    }

    @Test
    public void checkIfSortStringsCorrectly() {
        String[] li = {"abc", "def", "jjk", "aaa", "åøæ"};
        String[] li2 = {"aaa", "abc", "def", "jjk", "åøæ"};

        GenericMergeSort.mergeSort(li);

        assertTrue(Arrays.equals(li, li2));
    }

}