package binarytree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeTest {

	Random rand = new Random();

	private static BinarySearchTree<Integer> bst;
	private static BinarySearchTree<String> bstString;

	@BeforeEach
	void setUp() {
		bst = new BinarySearchTree<>();
		bstString = new BinarySearchTree<>();
	}

	@Test
	@DisplayName("gets correct height")
	void shouldReturnCorrectHeightForTree() {

		bst.insert(10);
		bst.insert(5);
		bst.insert(7);
		bst.insert(12);
		bst.insert(11);
		bst.insert(13);

		assertEquals(3, bst.height());
	}

	@Test
	@DisplayName("find correct number")
	void shouldSearchCorrectlyForGivenNumber() {

		for (int i = 0; i < 100; i++) {
			bst.insert(rand.nextInt(999));
		}
		bst.insert(1238);

		assertTrue(bst.search(1238));
	}

	@Test
	@DisplayName("correctly counts amount of leaves")
	void shouldCorrectlyCountLeavesOfTree() {

		bst.insert(4);
		bst.insert(10);
		bst.insert(8);
		bst.insert(9);
		bst.insert(1);
		bst.insert(2);

		assertEquals(2, bst.getLeafCount());
	}

	@Test
	@DisplayName("correctly counts amount of non-leaves in tree")
	void shouldCorrectlyCountNonLeavesInTree() {

		bst.insert(4);
		bst.insert(10);
		bst.insert(8);
		bst.insert(9);
		bst.insert(1);
		bst.insert(2);

		assertEquals(4, bst.getNonLeafCount());
	}

	@Test
	@DisplayName("finds the correct node in the correct order")
	void shouldFindTheCorrectNodeInOrder() {

		bst.insert(5);
		bst.insert(10);
		bst.insert(3);
		bst.insert(2);
		bst.insert(4);
		bst.insert(1);
		bst.insert(7);

		Integer[] ans = {5, 10, 7};
		ArrayList<Integer> ansArray = new ArrayList<>(Arrays.asList(ans));

		assertEquals(ansArray, bst.getPath(7));
	}

	@Test
	@DisplayName("finds the correct node and returns the object")
	void shouldReturnCorrectNodeObject() {

		bst.insert(5);
		bst.insert(10);
		bst.insert(3);
		bst.insert(2);
		bst.insert(4);
		bst.insert(1);
		bst.insert(7);

		assertEquals(5, bst.getNode(5).element);
	}

	@Test
	@DisplayName("works correctly with String")
	void shouldWorkCorrectlyWithStrings() {

		bstString.insert("one");
		bstString.insert("two");
		bstString.insert("three");

		assertEquals(3, bstString.getSize());
	}

	@Test
	@DisplayName("finds correct value with String")
	void shouldFindTheCorrectStringInTheBST() {

		bstString.insert("one");
		bstString.insert("two");
		bstString.insert("three");
		bstString.insert("hello");

		assertTrue(bstString.search("hello"));
	}

	@Test
	@DisplayName("handles an empty tree correctly")
	void shouldHandleEmptyTreeCorrectly() {
		assertEquals(0, bst.getLeafCount() + bst.getNonLeafCount());
	}
}