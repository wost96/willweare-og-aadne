package testing;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTestTest {

	MainTest test = new MainTest();

	@Test
	void testingWorksAsAdvertised() {
		assertEquals(1, test.runFunction());
	}
}