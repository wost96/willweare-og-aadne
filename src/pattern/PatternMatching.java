package pattern;

/**
 * Program tar inn to strenger: en streng og ett begrep å søke etter i strengen.
 * Løsningen skal være O(n), bare en iterasjon gjennom koden.
 * Ingen innebygde funksjoner som indexOf.
 */

public class PatternMatching {

	public static int find(String str, String pattern) {
		int patternIndex = -1;
		if(pattern.length() > str.length() || pattern.length() == 0)
			return -1;
		else {
			for (int i = 0, j = 0; i < str.length(); i++) {
				if(j >= pattern.length() || pattern.charAt(j) == str.charAt(i)) {
					if(j == 0) {
						patternIndex = i;
						// This check is needed in the situation that the string ends,
						// but the pattern isn't completed.
						if((i + pattern.length() - 1) > (str.length() - 1)) return -1;
					}
					j++;
				} else {
					if(j == pattern.length() - 1) return patternIndex;
					j = 0;
					patternIndex = -1;
				}
			}
		}
		return patternIndex;
	}
}
