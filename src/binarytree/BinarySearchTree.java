package binarytree;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;

public class BinarySearchTree<E extends Comparable<E>> implements Tree<E> {
  /** Root node for the tree. */
  private TreeNode<E> root;
  /** Size of the tree, number of nodes. */
  private int size = 0;
  /** Create a default binary tree. */
  BinarySearchTree() {
  }
  /**
   * Create a binary tree from an array of objects.
   *
   * @param objects Array of objects to use in creation of tree.
   */
  BinarySearchTree(E[] objects) {
    for (E object : objects) {
      insert(object);
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  //                            Interface Methods                            //
  /////////////////////////////////////////////////////////////////////////////
  public boolean search(E element) {
    TreeNode<E> current = root; // Start from the root
    while (current != null) {
      if (element.equals(current.element)) {
        return true; // Found element searched for
      } else if (element.compareTo(current.element) < 0) {
        // Search element is smaller than current node's element, go left
        current = current.left;
      } else {
        // Search element is bigger than current node'selement, go right
        current = current.right;
      }
    }
    return false; // Element not found in tree
  }
  public boolean insert(E element) {
    if (root == null) { // First element in Tree.
      root = createNewNode(element, null);
    } else {
      // Locate the parent node
      TreeNode<E> current = root;
      TreeNode<E> parent = current.parent;
      while (current != null) {
        // Go through the tree till we reach appropriate leaf node.parent = current;
        parent = current;
        if (element.equals(current.element)) {
          return false; // Duplicates not inserted
        } else if (element.compareTo(current.element) < 0) {
          // Inserted element is smaller than current node's element, go left
          current = current.left;
        } else {
          // Inserted element is bigger than current node's element, go right
          current = current.right;
        }
      }
      // Create the new node and attach it to the parent node
      if (element.compareTo(parent.element) < 0) {
        parent.left = createNewNode(element, parent);
      } else {
        parent.right = createNewNode(element, parent);
      }
    }
    size++;
    return true; // Element inserted
  }
  public boolean delete(E element) {
    // Locate the node to be deleted and also locate its parent node
    TreeNode<E> current = root;
    while (current != null) {
      if (element.equals(current.element)) {
        break; // Found the node
      } else if (element.compareTo(current.element) < 0) {
        // Deleted element is smaller than current node'selement, go left
        current = current.left;
      } else {
        // Deleted element is bigger than current node'selement, go right
        current = current.right;
      }
      current.parent = current;
    }
    if (current == null) {
      return false;
      // Element is not in the tree, nothing to remove.
    }
    if (current.left == null) {
      // Case 1: current has no left children
      if (current.parent == null) { // We delete root, simply replace root with right child
        root = current.right;
      } else {
        // Connect the parent with the right child
        if (element.compareTo(current.parent.element) < 0) {
          current.parent.left = current.right;
        } else {
          current.parent.right = current.right;
        }
      }
    } else {
      // Case 2: The current node has a left child
      // Locate the rightmost node in the left subtree of
      // the current node and also its parent
      TreeNode<E> rightMost = current.left;
      while (rightMost.right != null) {
        rightMost.parent = rightMost;
        rightMost = rightMost.right; // Keep going to the right
      }
      // Replace the element in current by the element in rightMost
      current.element = rightMost.element;
      // Eliminate rightmost node
      if (rightMost.parent.right == rightMost) {
        rightMost.parent.right = rightMost.left;
      } else { // Special case: parentOfRightMost == current
        rightMost.parent.left = rightMost.left;
      }
    }
    size--;
    return true; // Element removed
  }
  public void inOrder() {
    traverse(root, Traversal.IN_ORDER);
  }
  public void postOrder() {
    traverse(root, Traversal.POST_ORDER);
  }
  public void preOrder() {
    traverse(root, Traversal.PRE_ORDER);
  }
  public java.util.Iterator<E> iterator() {
    return new InOrderIterator();
  }
  public int getSize() {
    return size;
  }
  /////////////////////////////////////////////////////////////////////////////
  //                           Additional Methods                            //
  /////////////////////////////////////////////////////////////////////////////
  /** Remove all elements from the tree */
  void clear() {
    root = null;
    size = 0;
  }
  /**
   * Returns the root of the tree.
   *
   * @return Root node of the tree.
   */
  private TreeNode<E> getRoot() {
    return root;
  }
  /**
   * Return the height of this binary tree. Height is the number of the nodes in
   * the longest path of the tree.
   *
   * @return Height og tree.
   */
  int height() {
    return height(root);
  }

  private int height(TreeNode<E> node) {
    if(node == null) return 0;

    int leftTree  = height(node.left);
    int rightTree = height(node.right);

    return 1 + Integer.max(leftTree, rightTree);
  }

  /**
   * Method for traversing the tree and displaying its nodes in a Depth First
   * manner.
   */
  void depthFirstTraversal() {
    LinkedList<TreeNode<E>> queue = new LinkedList<>();

    queue.add(root);
    while(!queue.isEmpty()) {
      TreeNode<E> node = queue.removeLast();
      System.out.println(node.element);
      if(node.left  != null) queue.add(node.left);
      if(node.right != null) queue.add(node.right);
    }
  }

  /**
   * Method for traversing the tree and displaying its nodes in a Breadth First
   * manner.
   */
  void breadthFirstTraversal() {
    LinkedList<TreeNode<E>> queue = new LinkedList<>();

    queue.add(root);
    while(!queue.isEmpty()) {
      TreeNode<E> node = queue.pop();
      System.out.println(node.element);
      if(node.left  != null) queue.add(node.left);
      if(node.right != null) queue.add(node.right);
    }

  }

  int getLeafCount() {
    return getLeafCount(root);
  }

  private int getLeafCount(TreeNode<E> node) {
    if(node == null) return 0;
    else {
      if(node.left == null && node.right == null)
        return 1;
      return getLeafCount(node.right) + getLeafCount(node.left);
    }
  }

  int getNonLeafCount() {
    return getNonLeafCount(root);
  }

  private int getNonLeafCount(TreeNode<E> node) {
    if(node == null || (node.left == null && node.right == null)) return 0;
    int n = 1;

    if(node.right != null)  n += getNonLeafCount(node.right);
    if(node.left  != null)  n += getNonLeafCount(node.left);

    return n;
  }

  TreeNode<E> getNode(E element) {
    if(root == null) return null;
    TreeNode<E> current = root;
    while(current != null) {
      if(current.element == element) return current;
      if(element.compareTo(current.element) == 1) {
        current = current.right;
      }
      else if(element.compareTo(current.element) == -1) {
        current = current.left;
      }
    }
    return null;
  }

  ArrayList<E> getPath(E element) {
    if(root == null) return new ArrayList<>();
    ArrayList<E> path = new ArrayList<>();

    TreeNode<E> current = root; // Start from the root
    path.add(root.element);
    while (current != null) {
      if (element.equals(current.element)) {
        return path;
      } else if (element.compareTo(current.element) < 0) {
        // Search element is smaller than current node's element, go left
        current = current.left;
        path.add(current.element);
      } else {
        // Search element is bigger than current node'selement, go right
        current = current.right;
        path.add(current.element);
      }
    }
    return path.get(path.size() - 1) == element ? path : new ArrayList<>();
    }


  /////////////////////////////////////////////////////////////////////////////
  //                             Helper Methods                              //
  /////////////////////////////////////////////////////////////////////////////
  /**
   * Helper method to create a TreeNode for a given element.
   *
   * @param element Element to be put into the TreeNode
   *
   * @return A TreeNode with specified element inside it
   */
  private TreeNode<E> createNewNode(E element, TreeNode<E> parent) {
    return new TreeNode<>(element, parent);
  }
  /**
   * Helper method for general traversal from a subtree.
   *
   * @param node Which node to start traversal from.
   * @param mode Traversal mode, In-oder, Pre-oder or Post-order.
   */
  private void traverse(TreeNode<E> node, Traversal mode) {
    if (node == null) {
      return;
    }
    if (mode == Traversal.PRE_ORDER) {
      System.out.print(node.element + " ");
    }
    traverse(node.left, mode);
    if (mode == Traversal.IN_ORDER) {
      System.out.print(node.element + " ");
    }
    traverse(node.right, mode);
    if (mode == Traversal.POST_ORDER) {
      System.out.print(node.element + " ");
    }
  }
  private boolean isLeaf(TreeNode<E> node) {
    return node.right == null && node.left == null;
  }
  /////////////////////////////////////////////////////////////////////////////
  //                             Private Classes                             //
  /////////////////////////////////////////////////////////////////////////////
  static class TreeNode<T extends Comparable<T>> {
    T element;
    TreeNode<T> parent;
    TreeNode<T> left;
    TreeNode<T> right;
    TreeNode(T element, TreeNode<T> parent) {
      this.element = element;
      this.parent = parent;
    }
  }
  private class InOrderIterator implements java.util.Iterator<E> {
    // Store the elements in a list
    private ArrayList<E> list = new ArrayList<>();
    private int current = 0; // Point to the current element in list
    InOrderIterator() {
      inOrder(); // Traverse binary tree and store elements in list
    }
    /** In-order traversal from the root */
    private void inOrder() {
      inOrder(getRoot());
    }
    /** In-order traversal from a subtree */
    private void inOrder(TreeNode<E> root) {
      if (root == null) {
        return;
      }
      inOrder(root.left);
      list.add(root.element);
      inOrder(root.right);
    }
    /** Next element for traversing? */
    public boolean hasNext() {
      return current < list.size();
    }
    /** Get the current element and move cursor to the next. */
    public E next() {
      return list.get(current++);
    }
    /** Remove the current element and refresh the list. */
    public void remove() {
      delete(list.get(current)); // Delete the current element
      list.clear(); // Clear the list
      inOrder(); // Rebuild the list
    }
  }
  private enum Traversal {
    IN_ORDER, POST_ORDER, PRE_ORDER
  }
}